<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo Partido </title>
</head>

<body>
    <div width="10% " class="well text-center ">

        <h1>
            <i class="glyphicon glyphicon-plus"></i>
            Nuevo Partido


        </h1>
    </div>

    <form id="frm_nuevo_Partido" class="" enctype="multipart/form-data" action="<?php echo site_url("partidos/guardarPartido"); ?>" method="post">
        <!-- utilziar metodo post para mas seguridad  -->
        <!-- enctype lleva multiples archivos -->
        <div class="row">
        <div class="col-md-5">
            </div>
            <div class="col-md-2">
            <h4 class="text-center">Estadio:</h4>
                <select id="inputState" class="form-control" name="nombre_est_par_lr" id="nombre_est_par_lr" required>
                    <option selected>Elegir Estadio</option>
                    <?php if ($listadoEstadio) : ?>
                        <?php foreach ($listadoEstadio->result() as $estadioTemporal) : ?>
                            <option value="<?php echo $estadioTemporal->nombre_et_lr; ?>">
                                <?php echo $estadioTemporal->nombre_et_lr; ?>

                            </option>
                        <?php endforeach ?>
                    <?php endif ?>

                </select>
            </div>
            <div class="col-md-5">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">
            </div>

            <div class="col-md-2">
            <h4 class="text-center">Equipo 1:</h4>
                <select id="inputState" class="form-control" name="equipo1_par_lr" id="equipo1_par_lr" required>
                    <option selected>Elegir Equipo 1</option>
                    <?php if ($listadoEquipo) : ?>
                        <?php foreach ($listadoEquipo->result() as $estadioTemporal) : ?>
                            <option value="<?php echo $estadioTemporal->pais_eq_loro; ?>">
                                <?php echo $estadioTemporal->pais_eq_loro; ?>

                            </option>
                        <?php endforeach ?>
                    <?php endif ?>

                </select>
            </div>
            <div class="col-md-1">
                <p class="text-center">VS</p>
            </div>

            <div class="col-md-2">
            <h4 class="text-center">Equipo 2:</h4>
                <select id="inputState" class="form-control" name="equipo2_par_lr" id="equipo2_par_lr" required>
                    <option selected>Elegir Equipo 2</option>
                    <?php if ($listadoEquipo) : ?>
                        <?php foreach ($listadoEquipo->result() as $estadioTemporal) : ?>
                            <option value="<?php echo $estadioTemporal->pais_eq_loro; ?>">
                                <?php echo $estadioTemporal->pais_eq_loro; ?>

                            </option>
                        <?php endforeach ?>
                    <?php endif ?>

                </select>
            </div>

            <div class="col-md-4">
            </div>


        </div>
        <br>

        <div class="row">
            <div class="col-md-5">

            </div>
            <div class="col-md-2">
                <button type="submit" name="button" class="btn btn-primary">
                    <i class="glyphicon glyphicon-floppy-saved"></i>
                    Guardar
                </button>

                <a href="<?php echo site_url("partidos/index"); ?>" class="btn btn-danger">
                    <i class="glyphicon glyphicon-remove"></i>
                    Cancelar
                </a>
            </div>
            <div class="col-md-5">

            </div>
        </div>

    </form>
    <br>



</body>

</html>