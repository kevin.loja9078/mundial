<div class="row">
    <div class="col-md-12 text-center well">
        <h3>Actualizar Partido</h3>
    </div>
</div>
<div class="text-center">
    <a href="<?php echo site_url("partidos/index"); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-menu-left"></i>
        Volver
    </a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
    <?php if ($partidoEditar): ?>
        <form action="<?php echo site_url("partidos/procesarActualizacion"); ?>" method="post">
            
           
            <center>
                <input value="<?php echo $partidoEditar->id_par_lr; ?>" type="hidden" name="id_par_lr" method="post">

            </center>


            <br>
            <div class="row">
        <div class="col-md-5">
            </div>
            <div class="col-md-2">
                <h4 class="text-center">Estadio:</h4>
                <select id="inputState" class="form-control" name="nombre_est_par_lr" id="nombre_est_par_lr" required>
                    <option selected><?php echo $partidoEditar->nombre_est_par_lr; ?></option>
                    <?php if ($listadoEstadio) : ?>
                        <?php foreach ($listadoEstadio->result() as $estadioTemporal) : ?>
                            <option value="<?php echo $estadioTemporal->nombre_et_lr; ?>">
                                <?php echo $estadioTemporal->nombre_et_lr; ?>

                            </option>
                        <?php endforeach ?>
                    <?php endif ?>

                </select>
            </div>
            <div class="col-md-5">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">
            </div>

            <div class="col-md-2">
            <h4 class="text-center">Equipo 1:</h4>
                <select id="inputState" class="form-control" name="equipo1_par_lr" id="equipo1_par_lr" required>
                    <option selected><?php echo $partidoEditar->equipo1_par_lr; ?></option>
                    <?php if ($listadoEquipo) : ?>
                        <?php foreach ($listadoEquipo->result() as $estadioTemporal) : ?>
                            <option value="<?php echo $estadioTemporal->pais_eq_loro; ?>">
                                <?php echo $estadioTemporal->pais_eq_loro; ?>

                            </option>
                        <?php endforeach ?>
                    <?php endif ?>

                </select>
            </div>
            <div class="col-md-1">
                <p class="text-center">VS</p>
            </div>

            <div class="col-md-2">
            <h4 class="text-center">Equipo 2:</h4>
                <select id="inputState" class="form-control" name="equipo2_par_lr" id="equipo2_par_lr" required>
                    <option selected><?php echo $partidoEditar->equipo2_par_lr; ?></option>
                    <?php if ($listadoEquipo) : ?>
                        <?php foreach ($listadoEquipo->result() as $estadioTemporal) : ?>
                            <option value="<?php echo $estadioTemporal->pais_eq_loro; ?>">
                                <?php echo $estadioTemporal->pais_eq_loro; ?>

                            </option>
                        <?php endforeach ?>
                    <?php endif ?>

                </select>
            </div>

            <div class="col-md-4">
            </div>


        </div>
        <br>

        
        </div>
            <br>
            <div class="row">
                <div class="col-md-4">

                </div>
                <div class="col-md-7">
                    <button type="submit" name="button" class="btn btn-warning">
                        <i class="glyphicon glyphicon-floppy-open"></i>
                        Actualizar
                    </button>

                    <a href="<?php echo site_url("partidos/index"); ?>" class="btn btn-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        Cancelar
                    </a>
                </div>
                <br>
        </form>
        <?php else: ?>
        <div class="alert alert-danger">
            <b>No se encontro el partido</b>

        </div>
        <?php endif; ?>
    </div>
</div>
