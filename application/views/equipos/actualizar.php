<div class="row">
    <div class="col-md-12 text-center well">
        <h3>Actualizar Equipo</h3>
    </div>
</div>
<div class="text-center">
    <a href="<?php echo site_url("equipos/index"); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-menu-left"></i>
        Volver
    </a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
    <?php if ($equipoEditar): ?>
        <form action="<?php echo site_url("equipos/procesarActualizacion"); ?>" method="post">
            
           
            <center>
                <input value="<?php echo $equipoEditar->id_eq; ?>" type="hidden" name="id_eq" method="post">

            </center>


            <br>
            <div class="row">
                <div class="col-md-4 text-right">
                    <label for="">Pais:</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control " placeholder="Ingrese el nombre del pais" name="pais_eq"
                        value="<?php echo $equipoEditar->pais_eq; ?>" required>
                </div>
                <div class="col-md-4">

                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4 text-right">
                    <label for="">Continente:</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control " placeholder="Ingrese los dos apellidos completos"
                        name="continente_eq" value="<?php echo $equipoEditar->continente_eq; ?>" required>
                </div>
                <div class="col-md-4">

                </div>
            </div>
            <br>
            
            <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Fotografia Bandera:</label>
            </div>
            <div class="col-md-4">
                <input type="file" accept="image/png , image/jpeg " name="bandera_eq" id="bandera_eq" 
                " required>
                <!-- image/*  para todos los archivos de imagen  -->
            </div>
            <div class="preview">

            </div>
            <div class="col-md-4">

            </div>
        </div>
            <br>
            <div class="row">
                <div class="col-md-4">

                </div>
                <div class="col-md-7">
                    <button type="submit" name="button" class="btn btn-warning">
                        <i class="glyphicon glyphicon-floppy-open"></i>
                        Actualizar
                    </button>

                    <a href="<?php echo site_url("equipos/index"); ?>" class="btn btn-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        Cancelar
                    </a>
                </div>
                <br>
        </form>
        <?php else: ?>
        <div class="alert alert-danger">
            <b>No se encontro al equipo</b>

        </div>
        <?php endif; ?>
    </div>
</div>
