<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
</head>
<body>
    <br>
    <h2 class="well text-center">Gestion de Equipos</h2>
    
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-4 text-center">
        <a href="<?php echo site_url("equipos/nuevo");?>" class="btn btn-success">
                    <i class="glyphicon glyphicon-plus"></i>
                Nuevo Equipo
                </a>
        </div>
        <div class="col-md-4">

        </div>
    
    </div>
    <br>
    <div class="col-md-3">

        </div>
    <div class="col-md-6 text-center">
        <table id="tbl_equipos" class=" table table-stripped table-bordered table-hover">
            <thead>
                <th class="text-center">ID</th>
                <th class="text-center">Bandera</th>
                <th class="text-center">Pais</th>
                <th class="text-center">Continente</th>
                
               
                
                <th class="text-center">Acciones</th>
            </thead>
            <tbody>
                <?php if($listadoEquipo):?>
                <?php foreach ($listadoEquipo->result() as $equipoTemporal): ?>
                <tr>
                    <td class="textcenter"><?php echo $equipoTemporal->id_eq_loro;?></td>
                    <!-- fotografia -->
                    <td>
                        <?php if ($equipoTemporal->bandera_eq_loro!=""):?>
                        <a href="<?php echo base_url('uploads/equipos') . '/' . $equipoTemporal->bandera_eq_loro;?> " target="_blank">
                        <img width="75px" height="50px" id="fotografia" src="<?php echo base_url('uploads/equipos') . '/' . $equipoTemporal->bandera_eq_loro;?> " alt="Foto Equipo">

                        </a>
                        <?php else: ?>
                            N/A
                        <?php endif;?>

                        
                        <!-- se concatena la url de la image -->

                    </td>
                    <td class="textcenter"><?php echo $equipoTemporal->pais_eq_loro;?></td>
                    <td class="textcenter"><?php echo $equipoTemporal->continente_eq_loro;?></td>
                    
                    
                     
                    <td class="textcenter">
                        <a class="btn btn-primary glyphicon glyphicon-pencil" tooltip="sa" href="<?php echo site_url("equipos/actualizar");?>/<?php echo $equipoTemporal->id_eq_loro;?>"></a>
                        <a onclick="return confirm('Esta seguro de eliminar?')"class="btn btn-danger glyphicon glyphicon-trash" href="<?php echo site_url("equipos/borrar");?>/<?php echo $equipoTemporal->id_eq_loro;?>"></a>
                      
                    </td>
                </tr>
                <?php endforeach;?>
                <?php else:?>
                    <h3 class="text-center">No existen Equipos </h3>
                <?php endif;?>
                
                
            </tbody>
            
        </table>
    </div>
    <div class="col-md-3">

        </div>
   
    

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <script type="text/javascript">
        $('#tbl_equipos').DataTable();
    </script>
    <style>
        .tr{
            background-color: aquamarine;
        }
    </style>

</body>
</html>