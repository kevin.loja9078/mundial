<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo Equipo </title>
</head>

<body>
    <div width="10% " class="well text-center ">

        <h1>
            <i class="glyphicon glyphicon-plus"></i>
            Nuevo Equipo


        </h1>
    </div>

    <form id="frm_nuevo_Equipo" class="" enctype="multipart/form-data" action="<?php echo site_url("equipos/guardarEquipo"); ?>" method="post">
        <!-- utilziar metodo post para mas seguridad  -->
        <!-- enctype lleva multiples archivos -->
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Pais:</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el nombre del pais" name="pais_eq_loro" id="pais_eq_loro">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Continente :</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el continente del pais" name="continente_eq_loro" id="continente_eq_loro">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Fotografia Bandera:</label>
            </div>
            <div class="col-md-4">
                <input type="file" accept="image/png , image/jpeg " name="bandera_eq_loro" id="bandera_eq_loro" required>
                <!-- image/*  para todos los archivos de imagen  -->
            </div>
            <div class="preview">

            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-7">
                <button type="submit" name="button" class="btn btn-primary">
                    <i class="glyphicon glyphicon-floppy-saved"></i>
                    Guardar
                </button>

                <a href="<?php echo site_url("equipos/index"); ?>" class="btn btn-danger">
                    <i class="glyphicon glyphicon-remove"></i>
                    Cancelar
                </a>
            </div>
            <div class="col-md-4">

            </div>
        </div>

    </form>
    <br>

    

</body>

</html>