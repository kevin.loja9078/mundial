<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <br>
    <h2 class="well text-center">Gestion de Estadios</h2>
    
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-4 text-center">
        <a href="<?php echo site_url("estadios/nuevo");?>" class="btn btn-success">
                    <i class="glyphicon glyphicon-plus"></i>
                Nuevo Estadio
                </a>
        </div>
        <div class="col-md-4">

        </div>
    
    </div>
    <br>
    <div class="col-md-3">

        </div>
    <div class="col-md-6 text-center">
        <table id="tbl_estadios" class=" table table-stripped table-bordered table-hover">
            <thead>
                <th class="text-center">ID</th>
                <th class="text-center">Nombre</th>
                <th class="text-center">Capacidad</th>
                
                
               
                
                <th class="text-center">Acciones</th>
            </thead>
            <tbody>
                <?php if($listadoEstadio):?>
                <?php foreach ($listadoEstadio->result() as $estadioTemporal): ?>
                <tr>
                    <td class="textcenter"><?php echo $estadioTemporal->id_et_lr;?></td>
                    <!-- fotografia -->
                    <td class="textcenter"><?php echo $estadioTemporal->nombre_et_lr;?></td>

                    <td class="textcenter"><?php echo $estadioTemporal->capacidad_et_lr;?></td>
                    
                    
                     
                    <td class="textcenter">
                        <a class="btn btn-primary glyphicon glyphicon-pencil" tooltip="sa" href="<?php echo site_url("estadios/actualizar");?>/<?php echo $estadioTemporal->id_et_lr;?>"></a>
                        <a onclick="return confirm('Esta seguro de eliminar?')"class="btn btn-danger glyphicon glyphicon-trash" href="<?php echo site_url("estadios/borrar");?>/<?php echo $estadioTemporal->id_et_lr;?>"></a>
                      
                    </td>
                </tr>
                <?php endforeach;?>
                <?php else:?>
                    <h3 class="text-center">No existen Estadios </h3>
                <?php endif;?>
                
                
            </tbody>
            
        </table>
    </div>
    <div class="col-md-3">

        </div>
   
    

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <script type="text/javascript">
        $('#tbl_estadios').DataTable();
    </script>
    <style>
        .tr{
            background-color: aquamarine;
        }
    </style>

</body>
</html>