<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo Estadio </title>
</head>

<body>
    <div width="10% " class="well text-center ">

        <h1>
            <i class="glyphicon glyphicon-plus"></i>
            Nuevo Estadio


        </h1>
    </div>

    <form id="frm_nuevo_Estadio" class="" enctype="multipart/form-data" action="<?php echo site_url("estadios/guardarEstadio"); ?>" method="post">
        <!-- utilziar metodo post para mas seguridad  -->
        <!-- enctype lleva multiples archivos -->
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Nombre :</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el nombre del estadio" name="nombre_et_lr" id="nombre_et_lr">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Capacidad :</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese la Capacidad del Estadio" name="capacidad_et_lr" id="capacidad_et_lr">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-7">
                <button type="submit" name="button" class="btn btn-primary">
                    <i class="glyphicon glyphicon-floppy-saved"></i>
                    Guardar
                </button>

                <a href="<?php echo site_url("estadios/index"); ?>" class="btn btn-danger">
                    <i class="glyphicon glyphicon-remove"></i>
                    Cancelar
                </a>
            </div>
            <div class="col-md-4">

            </div>
        </div>

    </form>
    <br>

    

</body>

</html>