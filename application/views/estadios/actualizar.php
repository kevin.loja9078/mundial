<div class="row">
    <div class="col-md-12 text-center well">
        <h3>Actualizar Estadio</h3>
    </div>
</div>
<div class="text-center">
    <a href="<?php echo site_url("estadios/index"); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-menu-left"></i>
        Volver
    </a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
    <?php if ($estadioEditar): ?>
        <form action="<?php echo site_url("estadios/procesarActualizacion"); ?>" method="post">
            
           
            <center>
                <input value="<?php echo $estadioEditar->id_et_lr; ?>" type="hidden" name="id_et_lr" method="post">

            </center>


            <br>
            <div class="row">
                <div class="col-md-4 text-right">
                    <label for="">Nombre:</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control " placeholder="Ingrese el nombre del Estadio" name="nombre_et_lr"
                        value="<?php echo $estadioEditar->nombre_et_lr; ?>" required>
                </div>
                <div class="col-md-4">

                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4 text-right">
                    <label for="">Capacidad:</label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control " placeholder="Ingrese la Capacidad del Estadio"
                        name="capacidad_et_lr" value="<?php echo $estadioEditar->capacidad_et_lr; ?>" required>
                </div>
                <div class="col-md-4">

                </div>
            </div>
            <br>
            
            
            <div class="col-md-4">

            </div>
        </div>
            <br>
            <div class="row">
                <div class="col-md-4">

                </div>
                <div class="col-md-7">
                    <button type="submit" name="button" class="btn btn-warning">
                        <i class="glyphicon glyphicon-floppy-open"></i>
                        Actualizar
                    </button>

                    <a href="<?php echo site_url("estadios/index"); ?>" class="btn btn-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        Cancelar
                    </a>
                </div>
                <br>
        </form>
        <?php else: ?>
        <div class="alert alert-danger">
            <b>No se encontro el estadio</b>

        </div>
        <?php endif; ?>
    </div>
</div>
