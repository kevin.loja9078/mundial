<div class="row">
    <div class="col-md-12 text-center well">
        <h3>Actualizar Jugador</h3>
    </div>
</div>
<div class="text-center">
    <a href="<?php echo site_url("jugadores/index"); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-menu-left"></i>
        Volver
    </a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
    <?php if ($jugadorEditar): ?>
        <form action="<?php echo site_url("jugadores/procesarActualizacion"); ?>" method="post">
            
           
            <center>
                <input value="<?php echo $jugadorEditar->id_jug_lr; ?>" type="hidden" name="id_jug_lr" method="post">

            </center>


            <br>
            <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Nombre:</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el nombre del jugador" name="nombre_jug_lr" id="nombre_jug_lr"
                value="<?php echo $jugadorEditar->nombre_jug_lr; ?>">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Apellido :</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el apellido del jugador" name="apellido_jug_lr" id="apellido_jug_lr"
                value="<?php echo $jugadorEditar->apellido_jug_lr; ?>">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Equipo :</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el equipo del jugador" name="equipo_jug_lr" id="equipo_jug_lr"
                value="<?php echo $jugadorEditar->equipo_jug_lr; ?>">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Numero:</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el numero del Jugador" name="numero_jug_lr" id="numero_jug_lr"
                value="<?php echo $jugadorEditar->numero_jug_lr; ?>">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        
        <br>
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-7">
                    <button type="submit" name="button" class="btn btn-warning">
                        <i class="glyphicon glyphicon-floppy-open"></i>
                        Actualizar
                    </button>

                    <a href="<?php echo site_url("estadios/index"); ?>" class="btn btn-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        Cancelar
                    </a>
                </div>
            <div class="col-md-4">

            </div>
        </div>

        </form>
        <?php else: ?>
        <div class="alert alert-danger">
            <b>No se encontro al jugador</b>

        </div>
        <?php endif; ?>
    </div>
</div>
