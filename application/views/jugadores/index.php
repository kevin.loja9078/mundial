<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
</head>
<body>
    <br>
    <h2 class="well text-center">Gestion de Jugadores</h2>
    
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-4 text-center">
        <a href="<?php echo site_url("jugadores/nuevo");?>" class="btn btn-success">
                    <i class="glyphicon glyphicon-plus"></i>
                Nuevo Jugador
                </a>
        </div>
        <div class="col-md-4">

        </div>
    
    </div>
    <br>
    <div class="col-md-3">

        </div>
    <div class="col-md-6 text-center">
        <table id="tbl_jugadores" class=" table table-stripped table-bordered table-hover">
            <thead>
                <th class="text-center">ID</th>
                <th class="text-center">Nombre</th>
                <th class="text-center">Apellido</th>
                <th class="text-center">Equipo</th>
                <th class="text-center">Numero</th>
                <th class="text-center">Foto</th>
                
               
                
                <th class="text-center">Acciones</th>
            </thead>
            <tbody>
                <?php if($listadoJugador):?>
                <?php foreach ($listadoJugador->result() as $jugadorTemporal): ?>
                <tr>
                    <td class="textcenter"><?php echo $jugadorTemporal->id_jug_lr;?></td>
                    <td class="textcenter"><?php echo $jugadorTemporal->nombre_jug_lr;?></td>
                    <td class="textcenter"><?php echo $jugadorTemporal->apellido_jug_lr;?></td>
                    <td class="textcenter"><?php echo $jugadorTemporal->equipo_jug_lr;?></td>
                    <td class="textcenter"><?php echo $jugadorTemporal->numero_jug_lr;?></td>

                    <!-- fotografia -->
                    <td>
                        <?php if ($jugadorTemporal->foto_jug_lr!=""):?>
                        <a href="<?php echo base_url('uploads/jugadores') . '/' . $jugadorTemporal->foto_jug_lr;?> " target="_blank">
                        <img width="75px" height="50px" id="fotografia" src="<?php echo base_url('uploads/jugadores') . '/' . $jugadorTemporal->foto_jug_lr;?> " alt="Foto Jugador">

                        </a>
                        <?php else: ?>
                            N/A
                        <?php endif;?>

                        
                        <!-- se concatena la url de la image -->

                    </td>
                    
                     
                    <td class="textcenter">
                        <a class="btn btn-primary glyphicon glyphicon-pencil" tooltip="sa" href="<?php echo site_url("jugadores/actualizar");?>/<?php echo $jugadorTemporal->id_jug_lr;?>"></a>
                        <a onclick="return confirm('Esta seguro de eliminar?')"class="btn btn-danger glyphicon glyphicon-trash" href="<?php echo site_url("jugadores/borrar");?>/<?php echo $jugadorTemporal->id_jug_lr;?>"></a>
                      
                    </td>
                </tr>
                <?php endforeach;?>
                <?php else:?>
                    <h3 class="text-center">No existen Jugadores </h3>
                <?php endif;?>
                
                
            </tbody>
            
        </table>
    </div>
    <div class="col-md-3">

        </div>
   
    

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <script type="text/javascript">
        $('#tbl_jugadores').DataTable();
    </script>
    <style>
        .tr{
            background-color: aquamarine;
        }
    </style>

</body>
</html>