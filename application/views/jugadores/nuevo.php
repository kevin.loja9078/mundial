<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo Jugador </title>
</head>

<body>
    <div width="10% " class="well text-center ">

        <h1>
            <i class="glyphicon glyphicon-plus"></i>
            Nuevo Jugador


        </h1>
    </div>

    <form id="frm_nuevo_Jugador" class="" enctype="multipart/form-data" action="<?php echo site_url("jugadores/guardarJugador"); ?>" method="post">
        <!-- utilziar metodo post para mas seguridad  -->
        <!-- enctype lleva multiples archivos -->
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Nombre:</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el nombre del jugador" name="nombre_jug_lr" id="nombre_jug_lr">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Apellido :</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el apellido del jugador" name="apellido_jug_lr" id="apellido_jug_lr">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Equipo :</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el equipo del jugador" name="equipo_jug_lr" id="equipo_jug_lr">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Numero:</label>
            </div>
            <div class="col-md-4">
                <input required type="text" class="form-control " placeholder="Ingrese el numero del Jugador" name="numero_jug_lr" id="numero_jug_lr">
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 text-right">
                <label for="">Fotografia Jugador:</label>
            </div>
            <div class="col-md-4">
                <input type="file" accept="image/png , image/jpeg " name="foto_jug_lr" id="foto_jug_lr" required>
                <!-- image/*  para todos los archivos de imagen  -->
            </div>
            <div class="preview">

            </div>
            <div class="col-md-4">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-7">
                <button type="submit" name="button" class="btn btn-primary">
                    <i class="glyphicon glyphicon-floppy-saved"></i>
                    Guardar
                </button>

                <a href="<?php echo site_url("jugadores/index"); ?>" class="btn btn-danger">
                    <i class="glyphicon glyphicon-remove"></i>
                    Cancelar
                </a>
            </div>
            <div class="col-md-4">

            </div>
        </div>

    </form>
    <br>

    

</body>

</html>