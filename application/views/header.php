<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mundial</title>
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
	<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- Jquery Validate -->
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/jquery.validate.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/additional-methods.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/messages_es_AR.min.js');?>"></script>
  
      <!-- Imortacion de Datatables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>

    
</head>
<body>
<nav class="navbar navbar-light " style="background-color: #0044ff; ">
  

  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"  aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a style="color:white;"class="navbar-brand" href="<?php echo site_url("");?>">App Mundial</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Equipos<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url("equipos/index");?>">Listado de Equipos</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url("equipos/nuevo");?>">Nuevo Equipo</a></li>
          
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Partidos <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url("partidos/index");?>">Listado Partidos</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url("partidos/nuevo");?>">Nuevo Partido</a></li>
          
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Estadios <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url("estadios/index");?>">Listado de Estadios</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url("estadios/nuevo");?>">Nuevo Estadio</a></li>
          
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Jugadores <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url("jugadores/index");?>">Listado de Jugadores</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url("jugadores/nuevo");?>">Nuevo Jugador</a></li>
          
          </ul>
        </li>
        
        

      </ul>
      <form class="navbar-form navbar-left">
        
      </form>
      <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Link <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url("");?>">Listado </a></li>
            <li role="separator" class="divider"></li>
          
          </ul>
        </li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


<style>
  .navbar  a{
    color:white;
  }
  .navbar  li{
    color:black;
  }
  .navbar-header a{
    color:white;
  }
  
</style>
