<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Equipos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("equipo");
    }

    //renderiza vista equipos
    public function index()
    {
        $data["listadoEquipo"] = $this->equipo->obtenerTodos();
        $this->load->view('header');
        $this->load->view('equipos/index.php', $data);
        $this->load->view('footer');
    }

    public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('equipos/nuevo.php');
		$this->load->view('footer');
	}
    //funncion para capturar los valores del nuevo formulario
    public function guardarEquipo()
    {
        $datosNuevoEquipo = array(
            "pais_eq_loro" => $this->input->post('pais_eq_loro'),
            "continente_eq_loro" => $this->input->post('continente_eq_loro'),
            "bandera_eq_loro" => $this->input->post('bandera_eq_loro')

        );

        //Inicio del proceso de subida de las fotografias
        $this->load->library("upload");                             //!Activando la libreria de subida de archivos
        $new_name = "foto_" . time() . "_" . rand(1, 5000);         //!generando un nombre aleatorio
        $config['file_name'] = $new_name;                           //!Ruta donde se subiran los archivos
        $config['upload_path'] = FCPATH . 'uploads/equipos/';   //!Ruta que devuelve los archivos a su carpeta 
        $config['allowed_types'] = 'jpg|jpeg|png';                  //!pdf|docx
        $config['max_size']  = 2 * 1024;                              //!5mb tamaño de imagen
        $this->upload->initialize($config);                         //!inicializar la configuracion previa
        //!validando la subida de archivos
        if ($this->upload->do_upload("bandera_eq_loro")) {           //!Cambio del campo de foto a lo que vayas a poner en el formulario
            //?se subio correctamente
            $dataSubida = $this->upload->data();
            $datosNuevoEquipo["bandera_eq_loro"] = $dataSubida['file_name'];
        }

        //* Fin del proceso de subida de archivos


        print_r($datosNuevoEquipo);

        if ($this->equipo->insertar($datosNuevoEquipo)) {
            redirect('equipos/index.php');
        } else {
            echo "<h1>Error</h1>";
        }
    }

    //Funcion para eliminar equipos
    public function borrar($id_eq_loro)
    {
        if ($this->equipo->eliminarPorId($id_eq_loro)) { //!cambiaras el id
            redirect('equipos/index.php');
        } else {
            echo "Error al eliminar :(";
        }
    }
    //funcion para renderizar el formulario de eactualizacion
    public function actualizar($id)
    {
        $data["equipoEditar"] = $this->equipo->obtenerPorId($id);
        $this->load->view("header");
        $this->load->view("equipos/actualizar.php", $data);
        $this->load->view("footer");
    }
    //funcion para editar al equipo
    public function procesarActualizacion()
    {
        $datosEquipoEditado = array(
            "pais_eq_loro" => $this->input->post('pais_eq_loro'),
            "continente_eq_loro" => $this->input->post('continente_eq_loro'),
            "bandera_eq_loro" => $this->input->post('bandera_eq_loro')
        );
        
        //Capturar el id para los cambios en edicion
        
        $this->load->library("upload");                             //!Activando la libreria de subida de archivos
        $new_name = "foto_" . time() . "_" . rand(1, 5000);         //!generando un nombre aleatorio
        $config['file_name'] = $new_name;                           //!Ruta donde se subiran los archivos
        $config['upload_path'] = FCPATH . 'uploads/equipos/';   //!Ruta que devuelve los archivos a su carpeta 
        $config['allowed_types'] = 'jpg|jpeg|png';                  //!pdf|docx
        $config['max_size']  = 2 * 1024;                              //!5mb tamaño de imagen
        $this->upload->initialize($config);                         //!inicializar la configuracion previa
        //!validando la subida de archivos
        if ($this->upload->do_upload("bandera_eq_loro")) {           //!Cambio del campo de foto a lo que vayas a poner en el formulario
            //?se subio correctamente
            $dataSubida = $this->upload->data();
            $datosEquipoEditado["bandera_eq_loro"] = $dataSubida['file_name'];
        }
        print_r($datosEquipoEditado);
        $id = $this->input->post("id_eq_loro");//!cambiaras el id
        if ($this->equipo->actualizar($id, $datosEquipoEditado)) {
            redirect('equipos/index.php');
        } else {
            echo "<h1>Error</h1>";
        }


    }
}
