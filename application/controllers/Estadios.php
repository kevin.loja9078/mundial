<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Estadios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("estadio");
    }

    //renderiza vista estadios
    public function index()
    {
        $data["listadoEstadio"] = $this->estadio->obtenerTodos();
        $this->load->view('header');
        $this->load->view('estadios/index', $data);
        $this->load->view('footer');
    }

    public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('estadios/nuevo.php');
		$this->load->view('footer');
	}
    //funncion para capturar los valores del nuevo formulario
    public function guardarEstadio()
    {
        $datosNuevoEstadio = array(
            "nombre_et_lr" => $this->input->post('nombre_et_lr'),
            "capacidad_et_lr" => $this->input->post('capacidad_et_lr')

        );

        //Inicio del proceso de subida de las fotografias
       

        //* Fin del proceso de subida de archivos


        print_r($datosNuevoEstadio);

        if ($this->estadio->insertar($datosNuevoEstadio)) {
            redirect('estadios/index');
        } else {
            echo "<h1>Error</h1>";
        }
    }

    //Funcion para eliminar estadios
    public function borrar($id_jg_loro)
    {
        if ($this->estadio->eliminarPorId($id_jg_loro)) { //!cambiaras el id
            redirect('estadios/index');
        } else {
            echo "Error al eliminar :(";
        }
    }
    //funcion para renderizar el formulario de eactualizacion
    public function actualizar($id)
    {
        $data["estadioEditar"] = $this->estadio->obtenerPorId($id);
        $this->load->view("header");
        $this->load->view("estadios/actualizar", $data);
        $this->load->view("footer");
    }
    //funcion para editar al estadio
    public function procesarActualizacion()
    {
        $datosEstadioEditado = array(
            "nombre_et_lr" => $this->input->post('nombre_et_lr'),
            "capacidad_et_lr" => $this->input->post('capacidad_et_lr')

        );
        
        //Capturar el id para los cambios en edicion
        
                   //!inicializar la configuracion previa
        //!validando la subida de archivos
    
        
        print_r($datosEstadioEditado);
        $id = $this->input->post("id_et_lr");//!cambiaras el id
        if ($this->estadio->actualizar($id, $datosEstadioEditado)) {
            redirect('estadios/index');
        } else {
            echo "<h1>Error</h1>";
        }


    }
}
