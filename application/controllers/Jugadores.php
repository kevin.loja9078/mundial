<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jugadores extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("jugador");
    }

    //renderiza vista jugadores
    public function index()
    {
        $data["listadoJugador"] = $this->jugador->obtenerTodos();
        $this->load->view('header');
        $this->load->view('jugadores/index.php', $data);
        $this->load->view('footer');
    }

    public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('jugadores/nuevo.php');
		$this->load->view('footer');
	}
    //funncion para capturar los valores del nuevo formulario
    public function guardarJugador()
    {
        $datosNuevoJugador = array(
            "nombre_jug_lr" => $this->input->post('nombre_jug_lr'),
            "apellido_jug_lr" => $this->input->post('apellido_jug_lr'),
            "equipo_jug_lr" => $this->input->post('equipo_jug_lr'),
            "foto_jug_lr" => $this->input->post('foto_jug_lr'),
            "numero_jug_lr" => $this->input->post('numero_jug_lr')


        );
        $this->load->library("upload");                             //!Activando la libreria de subida de archivos
        $new_name = "foto_" . time() . "_" . rand(1, 5000);         //!generando un nombre aleatorio
        $config['file_name'] = $new_name;                           //!Ruta donde se subiran los archivos
        $config['upload_path'] = FCPATH . 'uploads/jugadores/';   //!Ruta que devuelve los archivos a su carpeta 
        $config['allowed_types'] = 'jpg|jpeg|png';                  //!pdf|docx
        $config['max_size']  = 2 * 1024;                              //!5mb tamaño de imagen
        $this->upload->initialize($config);                         //!inicializar la configuracion previa
        //!validando la subida de archivos
        if ($this->upload->do_upload("foto_jug_lr")) {           //!Cambio del campo de foto a lo que vayas a poner en el formulario
            //?se subio correctamente
            $dataSubida = $this->upload->data();
            $datosNuevoJugador["foto_jug_lr"] = $dataSubida['file_name'];
        }

        //* Fin del proceso de subida de archivos


        print_r($datosNuevoJugador);

        if ($this->jugador->insertar($datosNuevoJugador)) {
            redirect('jugadores/index.php');
        } else {
            echo "<h1>Error</h1>";
        }
    }

    //Funcion para eliminar jugadores
    public function borrar($id_jg_lr)
    {
        if ($this->jugador->eliminarPorId($id_jg_lr)) { //!cambiaras el id
            redirect('jugadores/index');
        } else {
            echo "Error al eliminar :(";
        }
    }
    //funcion para renderizar el formulario de eactualizacion
    public function actualizar($id)
    {
        $data["jugadorEditar"] = $this->jugador->obtenerPorId($id);
        $this->load->view("header");
        $this->load->view("jugadores/actualizar", $data);
        $this->load->view("footer");
    }
    //funcion para editar al jugador
    public function procesarActualizacion()
    {
        $datosJugadorEditado = array(
            "nombre_jug_lr" => $this->input->post('nombre_jug_lr'),
            "apellido_jug_lr" => $this->input->post('apellido_jug_lr'),
            "equipo_jug_lr" => $this->input->post('equipo_jug_lr'),
            "foto_jug_lr" => $this->input->post('foto_jug_lr'),
            "numero_jug_lr" => $this->input->post('numero_jug_lr')

        );
        
        //Capturar el id para los cambios en edicion
        
                   //!inicializar la configuracion previa
        //!validando la subida de archivos
    
        
        print_r($datosJugadorEditado);
        $id = $this->input->post("id_jg_lr");//!cambiaras el id
        if ($this->jugador->actualizar($id, $datosJugadorEditado)) {
            redirect('jugadores/index');
        } else {
            echo "<h1>Error</h1>";
        }


    }
}
