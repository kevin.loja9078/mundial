<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Partidos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("partido");
        $this->load->model("estadio");
        $this->load->model("equipo");
    }

    //renderiza vista partidos
    public function index()
    {
        $data["listadoPartido"] = $this->partido->obtenerTodos();
        $data["listadoEstadio"] = $this->estadio->obtenerTodos();
        $data["listadoEquipo"] = $this->equipo->obtenerTodos();

        $this->load->view('header');
        $this->load->view('partidos/index', $data);
        $this->load->view('footer');
    }

    public function nuevo()
	{
        $data["listadoPartido"] = $this->partido->obtenerTodos();
        $data["listadoEstadio"] = $this->estadio->obtenerTodos();
        $data["listadoEquipo"] = $this->equipo->obtenerTodos();

		$this->load->view('header');
		$this->load->view('partidos/nuevo.php',$data);
		$this->load->view('footer');
	}
    //funncion para capturar los valores del nuevo formulario
    public function guardarPartido()
    {
        $datosNuevoPartido = array(
            "nombre_est_par_lr" => $this->input->post('nombre_est_par_lr'),
            "equipo1_par_lr" => $this->input->post('equipo1_par_lr'),
            "equipo2_par_lr" => $this->input->post('equipo2_par_lr')

        );

        //Inicio del proceso de subida de las fotografias
       

        //* Fin del proceso de subida de archivos


        print_r($datosNuevoPartido);

        if ($this->partido->insertar($datosNuevoPartido)) {
            redirect('partidos/index');
        } else {
            echo "<h1>Error</h1>";
        }
    }

    //Funcion para eliminar partidos
    public function borrar($id_eq_loro)
    {
        if ($this->partido->eliminarPorId($id_eq_loro)) { //!cambiaras el id
            redirect('partidos/index');
        } else {
            echo "Error al eliminar :(";
        }
    }
    //funcion para renderizar el formulario de eactualizacion
    public function actualizar($id)
    {
        $data["partidoEditar"] = $this->partido->obtenerPorId($id);
        $data["listadoEstadio"] = $this->estadio->obtenerTodos();
        $data["listadoEquipo"] = $this->equipo->obtenerTodos();
        $this->load->view("header");
        $this->load->view("partidos/actualizar", $data);
        $this->load->view("footer");
    }
    //funcion para editar al partido
    public function procesarActualizacion()
    {
        $datosPartidoEditado = array(
            "nombre_est_par_lr" => $this->input->post('nombre_est_par_lr'),
            "equipo1_par_lr" => $this->input->post('equipo1_par_lr'),
            "equipo2_par_lr" => $this->input->post('equipo2_par_lr')
        );
        
        //Capturar el id para los cambios en edicion
        
       
        print_r($datosPartidoEditado);
        $id = $this->input->post("id_eq_loro");//!cambiaras el id
        if ($this->partido->actualizar($id, $datosPartidoEditado)) {
            redirect('partidos/index');
        } else {
            echo "<h1>Error</h1>";
        }


    }
}
