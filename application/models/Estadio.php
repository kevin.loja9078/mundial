<?php

    class Estadio extends  CI_Model{
        function __construct()
        {
            parent::__construct();
        }
    public function insertar($datos)
    {
        return $this->db->insert("estadios_lr",$datos);
    }
    //Funcion que consulta todos los estudiantes de la base de datos
    public function obtenerTodos(){
        $estadios=$this->db->get("estadios_lr");
        if ($estadios->num_rows()>0) {
            return $estadios;
        } else {
            return false; //cuando no existen datos
        }
        
    }
    //funcion para eliminar a un estudiante mediante su id
    public function eliminarPorId($id){
        $this->db->where("id_et_lr", $id); //?Cambiaras el id_est por el id que le pusiste en la base de datos 
        return $this->db->delete("estadios_lr");
    }
    //Consultando al estudainte por su id
    public function obtenerPorId($id){

        $this->db->where("id_et_lr",$id); //?Cambiaras el id_est por el id que le pusiste en la base de datos
        $estadios=$this->db->get("estadios_lr");
        if ($estadios->num_rows()>0) {
            return $estadios->row();// Por que solo existe un estudiante, debido a que el id no se puede repetir
        } else {
            return false;
        }
        
    } 
    //Proceso de actualizacion del estudiante
    public function actualizar($id,$datos){
        $this->db->where("id_et_lr",$id);  //?Cambiaras el id_est por el id que le pusiste en la base de datos
        return $this->db->update("estadios_lr",$datos);//union del elminar e insertar para la creacion de esta funcion

    }

}//Cierre de clases no borrar

