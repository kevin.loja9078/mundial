<?php

    class Equipo extends  CI_Model{
        function __construct()
        {
            parent::__construct();
        }
    public function insertar($datos)
    {
        return $this->db->insert("equipos_loro",$datos);
    }
    //Funcion que consulta todos los estudiantes de la base de datos
    public function obtenerTodos(){
        $equipo=$this->db->get("equipos_loro");
        if ($equipo->num_rows()>0) {
            return $equipo;
        } else {
            return false; //cuando no existen datos
        }
        
    }
    //funcion para eliminar a un estudiante mediante su id
    public function eliminarPorId($id){
        $this->db->where("id_eq_loro", $id); //?Cambiaras el id_est por el id que le pusiste en la base de datos 
        return $this->db->delete("equipos_loro");
    }
    //Consultando al estudainte por su id
    public function obtenerPorId($id){

        $this->db->where("id_eq_loro",$id); //?Cambiaras el id_est por el id que le pusiste en la base de datos
        $equipo=$this->db->get("equipos_loro");
        if ($equipo->num_rows()>0) {
            return $equipo->row();// Por que solo existe un estudiante, debido a que el id no se puede repetir
        } else {
            return false;
        }
        
    } 
    //Proceso de actualizacion del estudiante
    public function actualizar($id,$datos){
        $this->db->where("id_eq_loro",$id);  //?Cambiaras el id_est por el id que le pusiste en la base de datos
        return $this->db->update("equipos_loro",$datos);//union del elminar e insertar para la creacion de esta funcion

    }

}//Cierre de clases no borrar

