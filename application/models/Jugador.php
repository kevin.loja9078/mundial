<?php

    class Jugador extends  CI_Model{
        function __construct()
        {
            parent::__construct();
        }
    public function insertar($datos)
    {
        return $this->db->insert("jugadores_lr",$datos);
    }
    //Funcion que consulta todos los estudiantes de la base de datos
    public function obtenerTodos(){
        $jugador=$this->db->get("jugadores_lr");
        if ($jugador->num_rows()>0) {
            return $jugador;
        } else {
            return false; //cuando no existen datos
        }
        
    }
    //funcion para eliminar a un estudiante mediante su id
    public function eliminarPorId($id){
        $this->db->where("id_jug_lr", $id); //?Cambiaras el id_est por el id que le pusiste en la base de datos 
        return $this->db->delete("jugadores_lr");
    }
    //Consultando al estudainte por su id
    public function obtenerPorId($id){

        $this->db->where("id_jug_lr",$id); //?Cambiaras el id_est por el id que le pusiste en la base de datos
        $jugador=$this->db->get("jugadores_lr");
        if ($jugador->num_rows()>0) {
            return $jugador->row();// Por que solo existe un estudiante, debido a que el id no se puede repetir
        } else {
            return false;
        }
        
    } 
    //Proceso de actualizacion del estudiante
    public function actualizar($id,$datos){
        $this->db->where("id_jug_lr",$id);  //?Cambiaras el id_est por el id que le pusiste en la base de datos
        return $this->db->update("jugadores_lr",$datos);//union del elminar e insertar para la creacion de esta funcion

    }

}//Cierre de clases no borrar

